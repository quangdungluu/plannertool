const monthNames = ["Januar", "Februar", "März", "April", "Mai", "Juni",
  "July", "August", "September", "Oktober", "November", "Dezember"
];
let currentTime = new Date();
function updateTime() {
    let currentMonth = currentTime.getMonth();
    let currentYear = currentTime.getFullYear();
    updateMonthYearDisplay(currentMonth,currentYear);
    updateDaysOfMonth(currentMonth,currentYear);
}

function increaseMonth() {
    currentTime.setMonth(currentTime.getMonth() + 1);
    updateMonthYearDisplay(currentTime.getMonth(),currentTime.getFullYear());
    updateDaysOfMonth(currentTime.getMonth(),currentTime.getFullYear());
}

function decreaseMonth() {
    currentTime.setMonth(currentTime.getMonth() - 1);
    updateMonthYearDisplay(currentTime.getMonth(),currentTime.getFullYear());
    updateDaysOfMonth(currentTime.getMonth(),currentTime.getFullYear());
}

function updateMonthYearDisplay(currentMonth,currentYear) {
    displayMonth.innerHTML = monthNames[currentMonth];
    displayYear.innerHTML = currentYear;
}

function updateDaysOfMonth(month,year){
    let firstDay = new Date(year,month,1).getDay();
    let numberOfDays = new Date(year, month+1, 0).getDate();
    let day = 1;
    for (let counter = 0; counter <= 41; counter++) {
        let idOfDay = "day-" + counter;
        if(document.getElementById(idOfDay).classList.contains("visibility-hidden")){
            document.getElementById(idOfDay).classList.remove("visibility-hidden");
        }
        if(counter >= firstDay && counter < (numberOfDays+firstDay)){
            document.getElementById(idOfDay).innerHTML = ('0' + day).slice(-2);
            day++;
        } else {
            document.getElementById(idOfDay).innerHTML = '00';
            document.getElementById(idOfDay).classList.add("visibility-hidden");
        }
    }
}