<?php
    $inputDate = $_POST['inputDate'];
    $inputTime = $_POST['inputTime'];
    $inputMeeting = $_POST['inputMeeting'];

    $host = "localhost";
    $dbUsername = "root";
    $dbPassword = "";
    $dbName = "plannerdbtable";

    $conn = new mysqli($host, $dbUsername, $dbPassword, $dbName);

    if ($conn->connect_error) {
        die('Could not connect to the database.');
    } else {
        $Insert = "INSERT INTO plannerdbtable(DateMeeting, TimeMeeting, NameMeeting) values(?, ?, ?)";

        $stmt = $conn->prepare($Insert);
        $stmt->bind_param("sss",$inputDate, $inputTime, $inputMeeting);
    if ($stmt->execute()) {
        echo "New record inserted sucessfully.";
    }   else {
        echo $stmt->error;
    }
        $stmt->close();
        $conn->close();
    }
?>